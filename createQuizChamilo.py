#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import imp

print("WARNING: createQuizChamilo.py is deprecated. Use 'chamilotools create-quiz' instead.")
DIR = os.path.dirname(__file__)
chamilotools = imp.load_source(
    'chamilotools', os.path.join(DIR, 'chamilotools'))
chamilotools.main(['create-quiz'] + sys.argv[1:])

# Chamilo tools

Chamilo tools is a set of tools to interact with a
[Chamilo](https://chamilo.org/) server without using your browser.

Right now, the main commands are:

* `chamilotools upload-zip FILE.zip`: to upload the content of your course
  in one command.

* `chamilotools set-intro INTRO.html`: to set the introduction text
  from a local file.

* `chamilotools create-quiz`: to write quiz (Exercices)
  offline in a XLS file or a [Yaml](http://yaml.org/) file, and upload
  it to Chamilo in one command. Some
  [documentation](doc/create-quizz.md) is available.

It has initially been written by and for Ensimag teachers, but should
work for any Chamilo installation. It is tested both on Ensimag's
installation and on https://campus.chamilo.org/. Patches to make the
tools more generic and let them work in more contexts are welcome.

Chamilotools also contain a deprecated shell script `setChamilo` that
does the same as `upload-file` and `set-intro`.

# Installation and first step

**NEW** since version 1.0, `chamilotools` uses python3, not anymore python2.7

To install Chamilotools:

* Make sure you have Python 3.8.2+ installed.

* Install dependencies:

  ```
  pip3 install -r requirements.txt
  ```

* Download Chamilotools
  (`git clone https://gitlab.com/chamilotools/chamilotools.git` or
  download ZIP [here](https://gitlab.com/chamilotools/chamilotools/repository/archive.zip?ref=master)).

* Run `./chamilotools --help`.

# Configuration

chamilotools uses two configuration files:

* A per-user configuration file, located in
  `~/.config/chamilotools/config.yml`, meant to store your username.
  
* A per-project configuration file, located in `./.chamilotools.yml`,
  `../.chamilotools.yml`, `../../.chamilotools.yml`, etc. It is meant
  to store the course name and the Chamilo instance.

Configuration files are Yaml files. Currently, the only content
allowed in configuration files is (insert actual value instead of
`...` obviously):

```
username: ...
instance: ...
course: ...
```

These 3 keys provide default values for, respectively, `-u`,
`--instance` and `-c`. When the command-line options are specified,
the corresponding value from the configuration file is ignored.

# Creating Quizz (or test, or exercices)

Introduction: [create-quizz.md](doc/create-quizz.md)

The Yaml format: [yaml-syntax.md](doc/yaml-syntax.md)

Different syntax available in quiz: [syntax.md](doc/syntax.md)

Types of questions: [question-types.md](doc/question-types.md)

# Commands Related to Quiz

To get more information about each command, run
`chamilotools <command> --help`.

* `chamilotools create-quiz`: Upload a quiz (exercise) from a local file.
  See [doc/create-quizz.md](doc/create-quizz.md) for details.

* `chamilotools list-quiz`: List quiz available on a course.

* `chamilotools delete-quiz`: Delete a quiz remotely from the
  command-line. Specify which quiz to delete with `--exercise-id` or
  `--all` (dangerous ...).

* `chamilotools download-quiz`: Downloads the quiz in XML format. Note
  that chamilotools can currently not read this XML file.

* `chamilotools check-quiz`: Checks a quiz for syntax errors, without
  sending it to Chamilo.

# Commands Related to Introduction

* `chamilotools set-intro INTRO.html`: to set the introduction text
  from a local file.

* `chamilotools get-intro`: Get the introduction text from Chamilo.

# Commands Related to Documents

* `chamilotools upload-zip FILE.zip`: to upload the content of your
  course in one command. Warning: on most versions of Chamilo, if the
  destination directory already exist, the new is unziped in a new
  location (e.g. the ZIP file contains `my_directory`, but
  `my_directory` already exists: the new files will be created in
  `my_directory_1` and the old files won't be modified). You may want
  to use the (dangerous) command `chamilotools delete-doc` before
  using `upload-zip`. More [here](https://support.chamilo.org/issues/8416).

* `chamilotools upload-file FILE`: to upload a single file to Chamilo.
  If the file already exists on Chamilo, the new file will overwrite
  the old one.

* `chamilotools delete-doc FILE`: Delete the file `FILE` on Chamilo.
  The command can currently only delete files at the toplevel
  directory (not in subdirectories).
  Warning: this is an experimental feature and may be buggy (i.e. we
  try hard not to, but it may delete the wrong file). As a safety
  measure, `delete-doc` does a first pass where it looks for the
  filename in a relatively strict way. Use `--fuzzy-file-name` to
  allow a second pass where only the basename is used. For example,
  `delete-doc --fuzzy-file-name index` will delete `index.html`, but
  may also delete `index.pdf` or any file whose basename is `index`.
  Another safety measure is that if more than one matching file is
  found, `delete-doc` stops and does not delete anything.

* `chamilotools get-doc FILENAME`: Download the document `FILENAME`
  from Chamilo. `FILENAME` can be a full path like `path/to/file.txt`.

# Other commands

* `chamilotools version`: Get the version number of chamilotools.

* `chamilotools --help` (or `chamilotools <command> --help`): Get help.

# Troubleshooting and debugging

A few hints if Chamilotools do not work as you expect:

* Try running with `-vv` (or `-v -v`). You will see each URL visited
  by chamilotools.
  
* Try running with `--debug`. A browser will be launched on the
  current page if chamilotools encourters an error.
  
* If it is not sufficient, adding `br.launch_browser()` statements in
  the code to launch a browser at different steps.
  
* Don't hesitate to report bugs ;-).

# Scripting `chamilotools`

A common use of `chamilotools` is to use it in scripts, Makefiles,
cron job, ... A few advices on how to do this is available here:
[scripting.md](doc/scripting.md).

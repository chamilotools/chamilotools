# Chamilotools Syntax #

Chamilotools support several syntax in Quizz (in places where
formatting is allowed by Chamilo). The recommended way to set the
syntax is to set it directly within the quizz (in a Yaml quizz, use
the `syntax:` field either at the toplevel or within a question, or
the `syntaxAnswer:` field to specify only the syntax of the answer
within a question). If the syntax is not specified, `html` will be
used by default, and `mediawiki` will be used if the tool is launched
with the `-w` option.

Available syntaxes are:

* `html`: Apply no processing and pass the text directly to Chamilo,
  which will interpret is as HTML.

* `markdown`: Use markdown syntax that is supported by python [markdown package](http://pythonhosted.org/Markdown/)
  with `extra` extension (support code, table, ...).

  - code is highlight thanks to `highlight.js` with `ir-black` style

  - Math equations can be written as `<math>Equation LaTeX code</math>`
  
* `mediawiki`: Allow basic
  [mediawiki syntax](https://www.mediawiki.org/wiki/Help:Formatting):

  - mediawiki specific markup such as `''italic''`, `'''bold'''`.

  - HTML is still allowed, so to render `<`, `>` and `&`, you need
	to type respectively `&lt;`, `&gt;` and `&amp;`.

  - Math equations can be written as `<math>LaTeX code</math>`.

  - Images can be included with `[[File:PathToImage]]`. The URL
	generated will be of the form
	`http://URLCHAMILO/courses/COURSENAME/document/PathToImage`,
	i.e. the image file should be uploaded using the "documents"
	tool, and a relative link to this file should be given in
	`[[File:PathToImage]]`.

* `pre`: Useful for source code: this syntax renders all text as-is
  (technically, the text is HTML-escaped) and surrounds it with
  `<pre>`, which usually renders as a grey box around the text.

* `escape`: Like `pre`, but do not add `<pre>` tags.

* `pygments:`*language* (example: `pygments:python`): Consider the
  text as a piece of code in *language* and perform
  syntax-highlighting on it. Note: the whole text is
  syntax-highlighted, hence for T-type questions this usually plays
  badly with the separators used to specify the piece of text to be
  filled-in. A workaround is to use a separator which doesn't
  interact with syntax.

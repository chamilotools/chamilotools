# Available Types Of Questions #

Chamilotools allows entering most types of questions supported by
Chamilo (advanced question types like "image zones" are not).

Each type is identified by a short string (one letter for most common
question types) to be used in quiz (the `type:` field in Yaml syntax).
Below is the list of question types with its Chamilotools short name,
the icon used in Chamilo itself and Chamilotools specificities. In
some places (including Chamilo's manual), question types are identified
with a number, which is also provided here.

A complete example with all questions is available here:
[all_types.yml](../test/all_types.yml). The generated quizz should
look like this: [all_types.html](http://chamilotools.gitlab.io/chamilotools/all_types.html), but you may also
import the Yaml file into your own Chamilo instance and see.

## C: Multiple choice (Type 1) ##

![Multiple choice](img/mcua.png)

Question with multiple choice, but only one correct answer.

**Syntax:** Use a list with one item per question. Each item starts with
 `ok: <text>` (correct answer) or `ko: <text>` (incorrect answer) and
 can also contain an additional `feedback:` field that is used as a
 comment for this answer (explain why the answer is correct or why it
 isn't).

**Limitation:** On Chamilo, the user can give a score to each answer to
get "almost correct" answers (e.g. the correct answer gives 5 points,
but another answer gives 3 points because it's not far from OK).
Chamilotools sets the score to 0 for all incorrect answers.

**Example (Yaml):**

```Yaml
- type: C
  title: "Fractions (C: Multiple choice)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how the question is identical to the U-type question.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## M: Multiple answers (Type 2) ##

![Multiple answers](img/mcma.png)

**Syntax:** Similar to "multiple choice", but the user may select
 several options (at least one needs to be correct). The user gets
 positive points for each correct selected answer, and negative points
 for each incorrectly selected answer.

**Limitation:** Chamilotools does not allow a per-answer scoring. The
 score is given for the question, and Chamilotools will set this score
 for each answer (the score is made negative for incorrect answers).

**Example (Yaml):**

```Yaml
- type: M
  title: "Fractions (M: Multiple answers)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## T: Fill blanks or form (Type 3) ##

![Fill blanks or form](img/fill_in_blanks.png)

**Syntax:** Provide the text to fill-in as answer (it is convenient to
 use the `|` shortcut in Yaml syntax). Blanks are marked with a
 separator, which is `%` by default. For example
 `Enter the answer here: %42%.` will create a text with a blank
 instead of `42`. The separator can be changed using the `separator:`
 field.

**Field specific to these questions:**

* `separator` changes the separator used to delimit blanks (default:
  `%`). Possible values are:

  - `'['`: Blanks of the form `[...]`
  - `'{'`: Blanks of the form `{...}`
  - `'('`: Blanks of the form `(...)`
  - `'*'`: Blanks of the form `*...*`
  - `'#'`: Blanks of the form `#...#`
  - `'%'`: Blanks of the form `%...%`
  - `'$'`: Blanks of the form `$...$`

**Example (Yaml):**

```Yaml
- type: T
  score: 8
  title: "Completez le programme suivant (T: Fill blanks or form)"
  syntax: pre
  separator: '$'
  answers: |
    a = "Hello"
    n = len(a)
    b = ""
    while n > 0$:$
        b = b + a[n-1]
        n = n - 1
  feedbackTrue: Good!
  feedbackFalse: Hey, you should know this ...
```
**Caution :**
 * if your separator is `*` with  `mediawiki` syntax, be careful if  `*` begins a line , it will be transform to `<ul> <li>`
 * if your separator is used in a formula described with `<math>`, you will have problems

## A: Matching (Type 4) ##

![Matching](img/matching.png)

In this type of questions, the user must associate things together.
Points are given for each correct association.

**Syntax:** Provide the expected answer as a list of `A: B` elements,
 where `B` is meant to be associated to `A`.

**Example (Yaml):**

```Yaml
- type: A
  title: "A bit of history (A: Matching)"
  description: Associate events to dates
  answers:
    - 1623: Mechanical machine able to perform computation
    - 1939: First computer
    - 1975: Creation of Microsoft
    - 1974: Integrated circuits
    - 1976: Creation of Apple
    - 1981: Personal Computer
    - 1990: "Internet and hypertext links: the web" # quotes needed because of the : in the answer.
    - 1998: Creation of Google
    - 2004: Creation of Facebook
    - 2008: The most powerful computer reaches 1 PFlops (10 to the power 15 floating-point operation per seconds)
```

It is possible to have right-hand sides without corresponding
left-hand sides by having an answer without `:` like this:

```Yaml
- type: A
  score: 2
  title: Example association (<code>A</code>) question
  answers:
    - The '''A''' letter: first letter of the alphabet
    - The ''B'' letter: second letter of the alphabet
    - This does not match anyone.
```

## O: Open question (Type 5) ##

![Open question](img/open_answer.png)

Questions for which the answer is given in a free text area. The
expected answer is not given and there is no automatic correction for
these questions. If you know the expected answer (in a non-ambiguous
way), use "T: Fill blanks or form" instead.

**Syntax:** Just provide the usual fields (`type: O`, `title:`, ...).
 The `answers:` field is not required (and not used if given).

**Example (Yaml):**

```Yaml
- type: O
  title: "This is an open question (O: Open question)"
  description: Type whatever you want in the box below, the teacher will decide whether it is the correct answer.
```

## Image zones (Type 6) ##

![Image zones](img/hotspot.png)

*Unsupported by Chamilotools currently.*

## E: Exact Selection (Type 9) ##

![Exact Selection](img/mcmac.png)

Works like "M: Multiple answers", but the scoring is different: the
user needs to get the exact combination to get any points.

**Syntax:** Same as "M: Multiple answers".

**Example (Yaml):**

```Yaml
- type: E
  title: "Fractions (E: Exact Selection)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## U: Unique answer with unknown (Type 10) ##

![Unique answer with unknown](img/mcuao.png)

This questions are similar to "C: Multiple choice", with an additional
"Don't know" possible choice, that has a score hardcoded to 0.

**Note:** The author of this text does not understand the advantage of
these questions over the "C: Multiple choice". Ticking "Don't know",
the student gets 0 points. Ticking anything at random, the students
gets either 0 or more, so there's no point ticking "Unknown" ...

**Syntax:** Like "C: Multiple choice". Chamilotools will take care of
 adding the "Don't know" answer for you.

**Example (Yaml):**

```Yaml
- type: U
  title: "Fractions (U: Unique answer with unknown)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how the question is identical to the C-type question.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## MVF: Multiple answer true/false/don't know (Type 11) ##

![Multiple answer true/false/don't know](img/mcmao.png)

Question for which the answer is a list of statements. The user must
say which statement is true or false, and has a "don't know" option.
Correct true/false answer give a positive score, incorrect true/false
give a negative score (same score for all statements), and "don't
know" give 0 point.

**Syntax:** Same syntax as "M: multiple answers", plus the possibility
 to set Correct/Incorrect/DontKnow scores (see below).

**Fields specific to these questions:**

* `scoreCorrect` (positive integer): score to use for correct
  true/false answers. Defaults to `score`.

* `scoreIncorrect` (negative integer): score (penalty) to use for
  incorrect true/false answers. Defaults to -`score`.

* `scoreDontKnow` (integer): score to use for "don't know" answers.
  Defaults to 0.

**Example (Yaml):**

```Yaml
- type: MVF
  title: "Fractions (MVF: Multiple answer true/false/don't know)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. This question is almost identical to the M-type question except the type, but adds scoreCorrect, scoreIncorrect and scoreDontKnow optional fields.
  scoreCorrect: 3
  scoreIncorrect: -2
  scoreDontKnow: -1
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## MVFE: Combination true/false/don't-know (Type 12) ##

![Combination true/false/don't-know](img/mcmaco.png)

Similar to "MVF: Multiple answer true/false/don't know", but points
are given only if the exact combination is found.

**Note:** If "don't know" is picked for any statement, then the student
 gets no point. The subtlety is that on the student side, "MVF:
 Multiple answer true/false/don't know" and "MVFE: Combination
 true/false/don't-know" appear identical, so having "MVFE: Combination
 true/false/don't-know" questions from time to time can be seen as a
 way to discourage students from answering "don't know".

**Syntax:** Same as "M: multiple answers" an "E: Exact Selection".

**Example (Yaml):**

```Yaml
- type: MVFE
  title: "Fractions (MVFE: Combination true/false/don't-know)"
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. Note how this question is identical to the M-type question except the type.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## G: Global multiple answer (Type 14) ##

![Global multiple answer](img/mcmagl.png)

Similar to "M: Multiple answers", but the score for each answer is
computed automatically as **s = total score / number of true
statements** (same for each answer). The score is counted from the
maximum number of points and applying a penalty of **s** points for
each incorrect answer. If the computation gives a negative score, then
the actual score is set to 0.

For example, if the total score is 10, with 4 choices out of which 2
are true, then each incorrect answer will give a penalty of 10/2=5
points. If the user makes 3 mistakes, he or she gets -5 as total.

**Field specific to these questions:**

* `noNegative` ([boolean](http://yaml.org/type/bool.html)): if set to
  true, then check the tickbox "no negative score" in Chamilo. This
  tickbox changes the score computation. It counts positive points for
  each correct true answer, without counting any penalty for incorrect
  answers. A consequence of this is that ticking all tickboxes gives
  the maximum score. It's kind of a present for students, but the
  student doesn't know whether the option is active, hence ticking
  everything is a risky strategy for them.

**Syntax:** Same as "M: Multiple answers".

**Example (Yaml):**

```Yaml
- type: G
  title: "Fractions (G: Global multiple answer, noNegative)"
  noNegative: true # or not.
  description: Compute the value <math>\frac{1/2}{1/3}</math> in Python. This one has noNegative active, hence ticking everything gives you the maximum score.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: No, this one is equivalent to ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
      feedback: "Right (assuming you're using Python 3)"
    - ok: (1. / 2.) / (1. / 3.)
      feedback: "1. is the float, 1 is the integer but it doesn't matter here."
    - ko: (1 // 2) // (1 // 3)
      feedback: No, // is integer division in Python.
    - ko: 1 // 2 // 1 // 3
```

## Calculated question (Type 16) ##

![Calculated question](img/calculated_answer.png)

*Unsupported by Chamilotools currently.*

## Unique answer image (Type 17) ##

![Unique answer image](img/uaimg.png)

*Unsupported by Chamilotools currently.*

## Sequence ordering (Type 18) ##

![Sequence ordering](img/ordering.png)

*Unsupported by Chamilotools currently.*

## Match by dragging (Type 19) ##

![Match by dragging](img/matchingdrag.png)

*Unsupported by Chamilotools currently.*

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import importlib
from chamilolib.chamiloBrowser import ChamiloBrowser
from chamilolib.utilities import (
    display_html,
    AuthenticationError
)

MIMETEX = 0
MATHJAX = 1


class ChamiloInstance(object):
    def __init__(self, url):
        self.url_base = url
        self.br = None

    @staticmethod
    def build_instance(name):
        assert(name and name != '')
        klass_name = name + "ChamiloInstance"
        try:
            module = importlib.import_module(
                'chamilolib.' + klass_name)
            klass = getattr(module, klass_name)
            return klass()
        except (ImportError, AttributeError):
            pass
        if klass_name in globals():
            klass = globals()[klass_name]
            return klass()
        return None

    def create_browser(self, o):
        br = ChamiloBrowser()
        br.set_debug(o.debug)
        br.set_verbose(o.verbose)
        self.br = br
        return br

    def connect(self, o):
        br = self.create_browser(o)
        br.open(self.get_url_login())
        login_form = br.select_form('#formLogin')
        login_form.input({'login': o.username, 'password': o.password})
        response = br.submit_selected()
        html = response.soup.encode()
        if "user_password_incorrect" in br.get_url():
            display_html(html, "Invalid login or password")
            raise AuthenticationError()
        elif o.verbose >= 2:
            print("login successful")
        self.br = br
        return br

    def disconnect(self, br=None):
        if br is not None:
            br = self.br
        if br is None:
            return
        br.follow_link(url_regex="index.php\?logout=logout")

    def get_url_login(self):
        return self.get_url_base() + "/login"

    def get_url_base(self):
        """Base URL of the Chamilo instance"""
        return self.url_base

    def get_url_course(self, course):
        """URL for individual course"""
        return self.get_url_base() + "/courses/" + course + "/"

    def get_url_course_exercise(self, course):
        """URL for individual course exercise page"""
        return self.get_url_base() + "/main/exercice/exercice.php?cidReq=" + course

    def get_url_course_category(self, course):
        """URL for individual course exercise page"""
        return self.get_url_base() + "/main/exercice/tests_category.php?cidReq=" + course

    def get_entry_url_course(self, course):
        """Entry URL for individual course. This may differ from
        get_url_course() if the user is expected to visit another
        URL before entering the course."""
        return self.get_url_course(course)

    def get_math_engine(self):
        """Return the type of engine used to render Math."""
        return MATHJAX

    def get_url_mimetex(self):
        """If the math engine is MIMETEX, return the URL to the mimetex
        installation."""
        raise NotImplementedError()

    def has_double_escaping_bug(self):
        """Some instance of Chamilo un-HTML-escape the input we give them,
        hence we need to double-escape the text as a workaround.
        Return True if this is the case."""
        return False

    def text_field_width(self, nb_chars):
        """In 'text to be completed' type questions, we guess the size of
        the text field, in pixels, based on the expected input (in characters).
        Depending on the local CSS, the optimal width may vary.
        Return the width in pixels of the text input field."""
        # Based on display on campus.chamilo.org: 12 pixels padding +
        # 1 pixel width on each side = 26 pixels lost, plus 10 pixels
        # per character.
        return nb_chars * 10 + 26

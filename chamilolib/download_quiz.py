#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import zipfile
from io import StringIO

# import logging
# logger = logging.getLogger("mechanize")
# logger.addHandler(logging.StreamHandler(sys.stdout))
# logger.setLevel(logging.DEBUG)

from chamilolib.create_quiz_lib import (
    connectOrResetChamilo,
    CLIOptions
)

from chamilolib.utilities import (
    AuthenticationError
)

from mechanicalsoup import LinkNotFoundError


def downloadQuiz(o, br=None):
    br = connectOrResetChamilo(br, o)
    br.follow_link(url_regex=r"/exerci(c|s)e.php\?.*cidReq=" + o.courseName)
    try:
        resp = br.follow_link(url_regex='choice=exportqti2&exerciseId=' + str(o.exerciseId) + '($|&)')
    except LinkNotFoundError:
        print("No such exercice: " + str(o.exerciseId))
        return br, ""
    # We're downloading a ZIP file whose only content is an XML file.
    zip = StringIO(resp.content)
    zfp = zipfile.ZipFile(zip, "r")
    xml_name = next(a for a in zfp.namelist() if a.endswith('.xml'))
    return br, zfp.read(xml_name)


def cmd_download_quiz(o):
    o.read_password()
    br, xml = downloadQuiz(o)
    print(xml)


def main(args):
    global br
    o = CLIOptions(['download-quiz'] + args)
    cmd_download_quiz(o)


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except AuthenticationError:
        sys.exit(1)

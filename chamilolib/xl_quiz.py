#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import xlrd


def getCellStr(sheet, r, c):
    try:
        cell = sheet.cell(r, c)
    except:
        return ""
    if cell.ctype == 1:
        return cell.value
    else:
        return str(cell.value)


def openQuizXls(path):
    # Réouverture du classeur
    classeur = xlrd.open_workbook(path)
    # Récupération du nom de toutes les feuilles sous forme de liste
    nom_des_feuilles = classeur.sheet_names()
    # Récupération de la première feuille
    feuille = classeur.sheet_by_name(nom_des_feuilles[0])
    # Lecture des cellules:
    descriptionQuiz = {}
    asks = []
    if feuille.cell_value(0, 0) == 'Quiz':
        descriptionQuiz = {}
        descriptionQuiz["title"] = getCellStr(feuille, 0, 1)
        # descriptionQuiz["description"] = getCellStr(feuille, 0, 2)
        codeSyntax = getCellStr(feuille, 0, 2)
        descriptionQuiz["syntax"] = codeSyntax if codeSyntax else None
        descriptionQuiz["attempts"] = 2
        descriptionQuiz["feedbackFinal"] = "Retour global:"
        l = 1
        ask = {}
        nbTrue = 0
        while l < feuille.nrows:
            if getCellStr(feuille, l, 0) == "GlobalFeedback":
                descriptionQuiz["feedbackFinal"] = getCellStr(feuille, l, 1)
            if getCellStr(feuille, l, 0) == "Question":
                if ask:
                    if 'type' not in ask:
                        if 'answers' in ask:
                            if nbTrue == 1:
                                ask["type"] = "C"
                            else:
                                ask["type"] = "G"
                        else:
                            ask["type"] = "O"
                    asks.append(ask)
                ask = {}
                nbTrue = 0
                ask["answers"] = []
                try:
                    ask["category"] = getCellStr(feuille, l, 3)
                except IndexError:
                    ask["category"] = ""
                ask["type"] = getCellStr(feuille, l, 2)
                ask["title"] = getCellStr(feuille, l, 1)
                ask["description"] = ""
                ask["score"] = 5
                ask["feedback"] = "Bravo!"
                ask["feedbackFalse"] = "Recommencez!"
            elif feuille.cell_value(l, 0) == "EnrichQuestion":
                ask["description"] = getCellStr(feuille, l, 1)
                codeSyntax = getCellStr(feuille, l, 3)
                if codeSyntax:
                    ask["syntaxDesc"] = codeSyntax
            elif feuille.cell_value(l, 0) == "Score":
                ask["score"] = float(getCellStr(feuille, l, 2))
            elif feuille.cell_value(l, 0) == "FeedbackTrue":
                ask["feedback"] = getCellStr(feuille, l, 1)
            elif feuille.cell_value(l, 0) == "FeedbackFalse":
                ask["feedbackFalse"] = getCellStr(feuille, l, 1)
            elif feuille.cell_value(l, 0)[0:6] == "Answer":
                codeSyntax = getCellStr(feuille, l, 3)
                if codeSyntax:
                    ask["syntaxAnswer"] = codeSyntax
                if ask.get("type") == "T":
                    ask["answers"] = getCellStr(feuille, l, 1)
                elif ask.get("type") == "A":
                    if feuille.cell_value(l, 1):
                        ask["answers"].append(
                            {'text': getCellStr(feuille, l, 1),
                             'correct': getCellStr(feuille, l, 2)})
                    else:
                        ask["answers"].append(
                            {'text': None,
                             'correct': getCellStr(feuille, l, 2)})
                else:
                    if getCellStr(feuille, l, 2).strip():
                        nbTrue += 1
                    ask["answers"].append({'text': getCellStr(feuille, l, 1),
                                           'correct': getCellStr(feuille, l, 2).strip() != ""})
                    # TODO: implement feedback for XLS quiz
            l += 1
            if l == feuille.nrows:
                if 'type' not in ask:
                    if "answers" in ask:
                        if nbTrue == 1:
                            ask["type"] = "C"
                        else:
                            ask["type"] = "G"
                    else:
                        ask["type"] = "O"
                asks.append(ask)
    return (descriptionQuiz, asks)

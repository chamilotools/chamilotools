# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import os
import yaml


def find_local_config_file():
    previous = None
    current = os.curdir
    while previous != current:
        candidate = os.path.join(current, '.chamilotools.yml')
        if os.path.exists(candidate):
            return candidate
        previous = current
        current = os.path.abspath(os.path.join(current, os.pardir))


def find_global_config_file():
    home = os.path.expanduser('~')
    xdg_config_home = (
        os.environ.get('XDG_CONFIG_HOME') or
        os.path.join(home, '.config'))
    candidate = os.path.join(xdg_config_home, 'chamilotools', 'config.yml')
    if os.path.exists(candidate):
        return candidate


def read_config():
    conf = {}
    global_file = find_global_config_file()
    if global_file:
        global_conf = yaml.load(open(global_file, 'r', encoding='utf8'), yaml.BaseLoader)
        conf.update(global_conf)
    local_file = find_local_config_file()
    if local_file:
        local_conf = yaml.load(open(local_file, 'r', encoding='utf8'), yaml.BaseLoader)
        conf.update(local_conf)
    return conf


def apply_config(o, conf=read_config()):
    if 'username' in conf and not o.username:
        o.username = conf['username']
    if 'instance' in conf and not o.instanceName:
        o.instanceName = conf['instance']
        o.set_instance()
    if 'course' in conf and not o.courseName:
        o.courseName = conf['course']
    for k in conf:
        if k not in ('username', 'instance', 'course'):
            print("Warning: unknown configuration variable '" + k + "'. Please check your config files.")
            print("Please check your configuration files")

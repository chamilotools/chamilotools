# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import sys


class LoadError(Exception):
    pass


class AuthenticationError(Exception):
    pass


def display_html(html, fallback):
    try:
        import html.parser
        h = html.parser.HTMLParser()
        import html2text
        print(html2text.html2text(h.unescape(html.decode('utf-8'))))
    except:
        print(fallback)


def encode(x):
    assert x is not None
    if isinstance(x, str):
        return x
    else:
        return str(x)


def toUnicode(x):
    if not isinstance(x, str):
        return x.decode('utf-8')
    else:
        return str(x)


def ensure_str(x):
    if isinstance(x, str):
        return x
    else:
        return str(x)


def error(s):
    sys.stderr.write("ERROR: " + s + "\n")
    sys.exit(1)


def warn(s):
    sys.stderr.write("WARNING: " + s + "\n")


def extract_confirmation(resp):
    # Extract the answer from Chamilo
    parent_div = resp.soup.find(id="course_tools")
    parent_div = parent_div or resp.soup.find(id="top_main_content")
    # On recent Chamilo, the confirmation is within #top_main_content
    # but outside #main_content.
    parent_div = parent_div or resp.soup.find(id="main_content")
    if parent_div:
        confirmation = parent_div.find("div", class_="confirmation-message")
    else:
        confirmation = ""
    confirmation = confirmation or resp.soup.find("div", class_="alert")
    return confirmation

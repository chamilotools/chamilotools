# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from chamilolib.create_quiz_lib import (
    connectOrResetChamilo,
)
from chamilolib.utilities import (
    extract_confirmation,
)


def file_get_contents(filename):
    with open(filename, encoding='utf8') as f:
        s = f.read()
    return s


def open_intro_form(o, br=None):
    br = connectOrResetChamilo(br, o)
    br.follow_link(url_regex=r"(^|/)(index|course_home)\.php\?.*cidReq=" +
                   o.courseName +
                   ".*&intro_cmd(Edit|Add)=1"
                   )
    form = br.select_form('#introduction_text')
    return br, form


def set_intro(o, br=None):
    br, form = open_intro_form(o, br)
    new_intro = file_get_contents(o.text_file)
    # TODO: submit a patch to mechanicalsoup to allow doing this directly
    # See https://github.com/hickford/MechanicalSoup/issues/42
    form.form.find("textarea", {"name": "intro_content"}).string = new_intro
    resp = br.submit_selected(btnName="intro_cmdUpdate")

    return extract_confirmation(resp).getText().strip()


def get_intro(o, br=None):
    br, form = open_intro_form(o, br)
    return form.form.textarea.getText()


def cmd_set_intro(o):
    o.read_password()
    response = set_intro(o)
    print(response)


def cmd_get_intro(o):
    o.read_password()
    intro = get_intro(o)
    if o.output_file:
        with open(o.output_file, 'w', encoding='utf8') as f:
            f.write(intro)
    else:
        print(intro)

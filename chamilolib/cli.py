# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import argparse
import getpass
import sys
from chamilolib.ChamiloInstance import ChamiloInstance


class CLIError(Exception):
    pass


class CLIOptions(argparse.Namespace):

    ADD = 'add'
    REMOVE_SAME = 'remove-same'
    CHECK = 'check'
    REMOVE_ORPHAN = 'remove-orphan'
    ADD_MODES = (ADD, REMOVE_SAME, CHECK, REMOVE_ORPHAN)

    def create_base_parser(self, prefix=''):
        base_parser = argparse.ArgumentParser(add_help=False)
        login = base_parser.add_argument_group("Identifying yourself and the Chamilo instance")
        debug = base_parser.add_argument_group("Options for debugging")

        # Global options
        login.add_argument(
            '-u', metavar='NAME',
            action='store', dest=prefix + 'username', default='',
            help="Chamilo user login")
        login.add_argument(
            '-c', metavar='COURSE',
            action='store', dest=prefix + 'courseName', default='',
            help="Chamilo course name")
        login.add_argument(
            '--url', metavar='URL',
            action='store', dest=prefix + 'urlBase', default=None,
            help="""
                 Use the installation of Chamilo available at a
                 particular URL. Not that Chamilotools may need
                 more information than the URL to work properly,
                 hence using --instance is prefered over --url.
                 """)
        login.add_argument(
            '--campus',
            action='store_true', dest=prefix + 'useCampus',
            # help="Use campus.chamilo.org instance"
            help=argparse.SUPPRESS
        )
        login.add_argument(
            '--instance', metavar='NAME',
            action='store', dest=prefix + 'instanceName', default='',  # TODO: default to None
            help="""
                 Use a particular instance of Chamilo.
                 available instances are GrenobleINP, GrenobleINPMathJax, UGA, UGAIUT2,
                 Campus (campus.chamilo.org). You may easily add your own
                 installation to the list.
                 """)
        login.add_argument(
            '--password-from-stdin',
            action='store_true', dest=prefix + 'passwordFromStdin',
            help="Read password from stdin, to allow reading from a pipe.")

        debug.add_argument(
            '--verbose', '-v',
            action='count', dest=prefix + 'verbose', default=0,
            help="display progress as it happens. Use multiple times for more verbose output.")
        debug.add_argument(
            '-d',
            action='store_true', dest=prefix + 'debugMode',
            help="Activate debug mode")
        debug.add_argument(
            '--debug',
            action='store_true', dest=prefix + 'debug',
            help="Activate second debug mode")  # TODO: merge with -d.
        return base_parser

    def __init__(self, argv=sys.argv[1:], stop_before_parse=False):
        super(CLIOptions, self).__init__()
        self.password = ''

        # Python 3.4's argparse does the right thing when a
        # parent=[...] points to the same parser in the toplever
        # parser and in the subcommands, but Python 2.7.9 does not:
        # the subcommand resets any values set before it, hence
        # chamilotools -u foo create-quiz does not work (-u foo is
        # taken into account and then forgotten), while chamilotools
        # create-quiz -u foo does work. Work around the issue by using
        # two base parsers with different dest=... and merge values
        # later.
        global_base_parser = self.create_base_parser('global_')
        base_parser = self.create_base_parser()

        # The real parser
        self.parser = argparse.ArgumentParser(
            prog='chamilotools',
            description="Upload Quiz (aka exercices, aka tests) from a local file to Chamilo.",
            parents=[global_base_parser])
        parser = self.parser

        # Subcommand processing
        # (must come after global options for parents=[...] to work)
        subparsers = parser.add_subparsers(
            metavar="COMMAND",
            dest='command',
            help="Tell Chamilotools what to do")

        # create-quiz Options
        parser_create_quiz = subparsers.add_parser(
            'create-quiz',
            help='Create a quiz from a local (XL or Yaml) file',
            parents=[base_parser])
        quiz = parser_create_quiz.add_argument_group("Global options for this quiz")
        upload = parser_create_quiz.add_argument_group("Control which quiz and how it is uploaded")
        deprecated = parser_create_quiz.add_argument_group("Deprecated options")

        quiz.add_argument(
            '-w',
            action='store_true', dest="wiki",
            help="Activate MediaWiki syntax. "
                 "See https://gitlab.com/chamilotools/chamilotools/blob/master/doc/syntax.md.")
        quiz.add_argument(
            '-p', metavar='PERCENT',
            action='store', dest='passPercent', default="0",  # TODO: default to 0
            help="percentage needed to succeed the test (0 by default)")
        quiz.add_argument(
            '-t', metavar='NB', type=int,
            action='store', dest='nb', default=0,
            help="Number of attempts allowed to the students (infinity by default)")
        quiz.add_argument(
            '-l', metavar='TIME_LIMIT',
            action='store', dest='timeLimit', default="0",  # TODO: default to 0
            help="time limit in minutes to pass the test (no limit by default)")
        quiz.add_argument(
            '-f',
            action='store_false', dest='feedback',
            help="Disable feedback")
        quiz.add_argument(
            '-a',
            action='store_const', const="0", dest='mode', default="2",
            help="Auto-evaluation mode: show score and expected answers")
        quiz.add_argument(
            '-A',
            action='store_const', const="5", dest='mode', default="5",
            help="Auto-evaluation mode: show score and student answers \
            but not expected answers")
        quiz.add_argument(
            '-s',
            action='store_const', const="1", dest='mode', default="2",
            help="Exam mode: Do not show score nor answers")
        quiz.add_argument(
            '-e',
            action='store_const', const="2", dest='mode', default="2",
            help="Exercise/Practice mode: Show score only, by category if at least one is used")
        quiz.add_argument(
            '-o',
            action='store_true', dest='onePage',
            help="All questions on one page")
        quiz.add_argument(
            '-r',
            action='store_true', dest='randomCategory',
            help="Random by category")
        quiz.add_argument(
            '--random-answers',
            action='store_true', dest='randomAnswers',
            help="Shuffle answers")

        deprecated.add_argument(
            '-i', metavar='FILE',
            action='append', dest='filenames', default=[],
            help="file containing the quiz "
                 "(use <quiz_files> as positional argument instead)")

        upload.add_argument(
            'quiz_file',
            action='store', nargs='*', default=[],
            help="file containing the quiz")
        upload.add_argument(
            '-m', metavar='MODE',
            action='store', dest='addMode', default="0",
            help="""
                 Mode to add:
                 add: always add the quiz (default);
                 remove-same: remove all quiz with the same name and add the new one;
                 check: add only if no quiz exists with the same name;
                 remove-orphan: remove orphans questions, remove all quiz with the same name
                 and add the new one, CAUTION : DON'T USE IF THE COURSE USES CATEGORIES.
                 """)
        upload.add_argument(
            '--dump-yaml',
            action='store_true', dest='dumpYaml',
            help="Dump a Yaml version of the quiz (useful to convert an XL quiz into a Yaml one)")
        upload.add_argument(
            '--view',
            action='store_true', dest='view',
            help="Launch browser on generated online quiz")
        upload.add_argument(
            '--hidden',
            action='store_true', dest='hidden',
            help="Create the quiz, but hide it from students")
        upload.add_argument(
            '--cleanup',
            action='store_true', dest='cleanUp',
            help="Only meaningful with --dump-yaml. "
                 "Dump a Yaml file where fields having their default values are omitted.")

        # delete-quiz Options
        parser_delete_quiz = subparsers.add_parser(
            'delete-quiz',
            help='Delete a quiz remotely',
            parents=[base_parser])
        parser_delete_quiz.add_argument(
            '--exercise-id', metavar='ID',
            action='store', dest='exerciseId', default=0, type=int,
            help="Specify an exercise identifier."
        )
        parser_delete_quiz.add_argument(
            '--all',
            action='store_true', dest='all',
            help="Delete ALL quiz in the course."
        )

        # download-quiz Options
        parser_download_quiz = subparsers.add_parser(
            'download-quiz',
            help='Download a quiz in XML format',
            parents=[base_parser])
        parser_download_quiz.add_argument(
            '--exercise-id', metavar='ID',
            action='store', dest='exerciseId', default=0, type=int,
            help="Specify an exercise identifier."
        )

        # list-quiz Options
        parser_list_quiz = subparsers.add_parser(
            'list-quiz',
            help='List all quiz in this course',
            parents=[base_parser])
        parser_list_quiz.add_argument(
            '--quiz-name', metavar='NAME',
            action='store', dest='quizName',
            help="List only quiz whose title is NAME"
        )

        # check-quiz Options
        parser_list_quiz = subparsers.add_parser(
            'check-quiz',
            help='Check a quiz file for syntax errors',
            parents=[base_parser])
        parser_list_quiz.add_argument(
            'quiz_file',
            action='store', nargs='*', default=[],
            help="file containing the quiz")

        # upload-zip Options
        parser_upload_zip = subparsers.add_parser(
            'upload-zip',
            help='Uploads documents from a ZIP file',
            parents=[base_parser])
        parser_upload_zip.add_argument(
            'zip_file',
            action='store',
            help="Local ZIP file to upload"
        )

        # upload-zip Options
        parser_upload_file = subparsers.add_parser(
            'upload-file',
            help='Upload a local document to Chamilo',
            parents=[base_parser])
        parser_upload_file.add_argument(
            'file',
            action='store',
            help="Local file to upload"
        )

        # delete-doc Options
        parser_delete_doc = subparsers.add_parser(
            'delete-doc',
            help='Deletes a document (file or directory) on Chamilo. EXPERIMENTAL AND DANGEROUS.',
            parents=[base_parser])
        parser_delete_doc.add_argument(
            'filename',
            action='store',
            help="Name of the file or directory to delete. No / allowed."
        )
        parser_delete_doc.add_argument(
            '--fuzzy-file-name',
            action='store_true',
            help="Try to find the file name regardless of its extension "
                 "(e.g. 'delete-doc index --fuzzy-file-name' to delete index.*)")

        # get-doc Options
        parser_delete_doc = subparsers.add_parser(
            'get-doc',
            help='Download a file on Chamilo.',
            parents=[base_parser])
        parser_delete_doc.add_argument(
            'filename',
            action='store',
            help="Name of the file to download. Use path/to/file.txt for full path."
        )
        parser_delete_doc.add_argument(
            '--output', metavar='FILE',
            action='store', dest='output_file',
            help="Write result to FILE"
        )

        # set-intro Options
        parser_set_intro = subparsers.add_parser(
            'set-intro',
            help='Sets the introduction text form a local text or HTML file',
            parents=[base_parser])
        parser_set_intro.add_argument(
            'text_file',
            action='store',
            help="Local text or HTML file containing the introduction text"
        )

        # get-intro Options
        parser_get_intro = subparsers.add_parser(
            'get-intro',
            help='Gets the introduction text form a local text or HTML file',
            parents=[base_parser])
        parser_get_intro.add_argument(
            '--output', metavar='FILE',
            action='store', dest='output_file',
            help="Write result to FILE"
        )

        # version
        subparsers.add_parser(
            'version',
            help='Display the version of chamilotools',
            parents=[base_parser])

        if stop_before_parse:
            return

        self.parse_and_check(argv, namespace=self)

    def parse_and_check(self, argv, namespace=None):
        if namespace is None:
            namespace = self

        self.filenames = []

        # Actually parse, now:
        self.parser.parse_args(argv, namespace=namespace)

        # Merge values from base_parser and global_base_parser
        def check_global_local_conflict(name):
            # local overrides global
            local = getattr(self, name, False)
            globl = getattr(self, 'global_' + name, False)
            if local:
                globl = local
            setattr(self, name, globl)

        check_global_local_conflict('username')
        check_global_local_conflict('courseName')
        check_global_local_conflict('urlBase')
        check_global_local_conflict('useCampus')
        check_global_local_conflict('instanceName')

        check_global_local_conflict('passwordFromStdin')
        check_global_local_conflict('debugMode')
        check_global_local_conflict('debug')

        def int_global_local_conflict(name):
            local = getattr(self, name, False)
            globl = getattr(self, 'global_' + name, False)
            if globl and local:
                setattr(self, name, globl + local)

        int_global_local_conflict('verbose')

        # Backward compatibility
        if hasattr(self, 'addMode'):
            aliases = {
                '0': self.ADD,
                '1': self.REMOVE_SAME,
                '2': self.CHECK,
                '3': self.REMOVE_ORPHAN,
            }
            if self.addMode in aliases:
                self.addMode = aliases[self.addMode]

        if self.command in ('create-quiz', 'check-quiz'):
            self.filenames.extend(self.quiz_file)
        self.set_instance()
        self.check()

    def set_instance(self):
        self.instance = None

        if self.useCampus:
            self.instanceName = 'Campus'

        if self.instanceName is not None and self.instanceName != '':
            name = self.instanceName
            self.instance = ChamiloInstance.build_instance(name)
            if self.instance is None:
                print("No such chamilo instance: %s" % name)
                raise CLIError()

        if self.instance is None and self.urlBase is not None:
            self.instance = ChamiloInstance(self.urlBase)

    def read_password(self):
        if self.passwordFromStdin:
            self.password = sys.stdin.readline()
            if self.password[-1] == '\n':
                self.password = self.password[:-1]
        else:
            self.password = getpass.getpass()

    def check(self):
        if hasattr(self, 'addMode') and self.addMode not in self.ADD_MODES:
            raise CLIError("Invalid add mode " + self.addMode + ".\n"
                           "Please chose one of: " + ' '.join(self.ADD_MODES))

    def usage(self):
        self.parser.print_help()

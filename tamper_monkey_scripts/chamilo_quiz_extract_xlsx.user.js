// ==UserScript==
// @name         Chamilo Exctract quiz results Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Construct a csv file to summarize quiz results
// @author       Sébastien Viardot
// @match        https://*/main/exercise/exercise.php?*
// @grant        none
// @require   http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @require   https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.6/xlsx.full.min.js
// https://bundle.run/xlsx@0.15.4?name=XLSX
// https://unpkg.com/xlsx@0.15.4/dist/shim.min.js
//  https://unpkg.com/xlsx@0.15.4/dist/xlsx.full.min.js
// ==/UserScript==
urlBase=document.URL.match(/https\:\/\/[^\/]+/)[0]
function genere_csv(){
    let eleves={};
    let exercices={}
    const nb_quiz=$("tr[id^='exercise_list_']").length
    let nb_traite=0
    $("tr[id^='exercise_list_']").each(
        (i,tr)=>{
            let exercise_id=$(tr).attr("id").substr(14)
            let titre=$(tr).find("img").attr("title")
            exercices[exercise_id]=titre
            let url=`${urlBase}/main/inc/ajax/model.ajax.php?a=get_exercise_results&exerciseId=${exercise_id}&filter_by_user=&_search=false&rows=10000&page=1&sidx=&sord=asc`
            $.get(url, res => {
                if (res.rows != undefined) {
                    res.rows.forEach( resultat => {
                        let nom=resultat.cell[1]
                        let prenom=resultat.cell[0]
                        let login=resultat.cell[2]
                        let duree=parseInt(resultat.cell[4])
                        let debut=resultat.cell[5]
                        let fin=resultat.cell[6]
                        let score_brut=resultat.cell[7]
                        let extract_score=score_brut.match(/\(([0-9.]+)\s\/\s([0-9.]+)\)/)
                        let score=-1
                        let bareme=1
                        if (extract_score) {
                            score=parseInt(extract_score[1])
                            bareme=parseInt(extract_score[2])
                        }
                        if (eleves[login] === undefined ){
                            eleves[login]={}
                        }
                        eleves[login].nom=nom
                        eleves[login].prenom=prenom
                        if (eleves[login][exercise_id] === undefined ){
                            eleves[login][exercise_id]={score_maxi:0,duree:0,nb_essais:0}
                        }
                        eleves[login][exercise_id].duree+=duree
                        eleves[login][exercise_id].nb_essais+=1
                        if ((score/bareme)>eleves[login][exercise_id].score_maxi){
                            eleves[login][exercise_id].score_maxi=score/bareme
                        }
                    })
                }
                nb_traite++
                if (nb_traite==nb_quiz) {
                    let data=[]
                    let titre="quiz"
                    let merged=[]
                    data.push(["login","nom","prenom","note finale"])
                    let qn=0
                    for (const q in exercices){
                        data[0].push(exercices[q],"","")
                        merged.push({s:{r:0, c:3*qn+4},e:{r:0,c:3*qn+4+2}});qn++
                    }
                    const logins_tries=Object.keys(eleves).sort()
                    logins_tries.forEach( e=> {
                        let row=[e,eleves[e].nom,eleves[e].prenom,""]
                        for (const q in exercices){
                            if (eleves[e][q] === undefined){
                                row.push(0,0,0)
                            } else {
                                row.push(eleves[e][q].score_maxi*20,eleves[e][q].duree,eleves[e][q].nb_essais)
                            }
                        }
                        data.push(row)
                    })
                    let wb=XLSX.utils.book_new()
                    let ws=XLSX.utils.aoa_to_sheet(data)
                    for (let i=2;i<(logins_tries.length+2);i++){
                        let formule=""
                        for (let qn=0;qn<Object.keys(exercices).length;qn++){
                            formule+=((qn>0)?"+":"")+`${XLSX.utils.encode_col(3*qn+4)}${i}`
                            ws[`${XLSX.utils.encode_col(3*qn+4)}${i}`].z="0.0"
                        }
                        formule=`(${formule})/${Object.keys(exercices).length}`
                        ws[`D${i}`]={  f: formule, z: "0.0" }
                    }
                    ws["!autofilter"]={ref:`A1:${XLSX.utils.encode_col(3*Object.keys(exercices).length+3)}1`}
                    ws["!merges"] = merged
                    XLSX.utils.book_append_sheet(wb,ws,"Synthese")
                    XLSX.writeFile(wb,`quiz_${document.URL.match(/cidReq=([^&]*)/)[1]}.xlsx`)
                }
            })
        })
}
(function() {
    'use strict';
    const b_genere=$("<button>Genere synthese</button>")
    b_genere.click(genere_csv)
    $("body").append(b_genere)
})();

# Présentation

Ce répertoiez rassemble un certain nombre de scripts javascript permettant d'ajouter des fonctionnalités a posteriori à certains chamilo.

Pour les utiliser : 

0. Préférer firefox à chrome...
1. Installer sur son navigateur le plugin TamperMonkey https://www.tampermonkey.net/
2. Ouvrir le tableau de bord de TamperMonkey dans son navigateur, cliquer sur [+] pour ajouter un nouveau script, et copier coller le code du script souhaité.
3. Et voilà votre fonctionnalité a posteriori est installée. 

# Les scripts 

## chamilo_quiz_Extract_response_xlsx.user.js 

Ce script permet de disposer d'un bouton supplémentaire [Genere Synthese] tout en bas de chamilo quand on est sur une page de synthèse des résultats à un quiz, pour récupérer dans un fichier Excel le détail de toutes les réponses des étudiants avec la réponse attendue, le calcul de la note. 

Exemple de page où le bouton sera ajouté (remplacer ENSIMAG3MMARCHI par le code du cours souhaité et 2388 par le code de l'exercice souhaité):

https://chamilo.grenoble-inp.fr/main/exercise/exercise_report.php?cidReq=ENSIMAG3MMARCHI&id_session=0&gidReq=0&gradebook=0&origin=&exerciseId=2388

- Permet de revenir sur le barème, de refaire une passe manuelle sur la correction, de corriger des questions ouvertes, idéal pour un examen via Chamilo.

## chamilo_quiz_extract_xlsx.user.js

Ce script permet de disposer d'un bouton supplémentaire [Genere Synthese] tout en bas de chamilo quand on est sur une page récapitulant tous les quizs d'une matière.Synthèse des notes à tous les quizs:  pour récupérer dans un fichier Excel les notes obtenues pour tous les étudiants à tous les quizs.

Exemple de page où le bouton sera ajouté (remplacer ENSIMAG3MMARCHI par le code du cours souhaité): 

https://chamilo.grenoble-inp.fr/main/exercise/exercise.php?cidReq=ENSIMAG3MMARCHI&gidReq=0&id_session=0

- Permet de récupérer d'un seul coup tous les résultats à tous les quizs pour tous les étudiants dans un seul fichier. Idéal pour récupérer les notes des quizs pendant une période de cours.




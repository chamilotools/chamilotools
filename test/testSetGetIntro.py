#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import os
TEST_DIR = os.path.dirname(__file__)
SCRIPT_DIR = os.path.join(os.path.dirname(__file__), "..")
SCRIPT = os.path.join(SCRIPT_DIR, "chamilotools")
sys.path.append(SCRIPT_DIR)

import unittest
from subprocess import Popen, PIPE
import getpass
from chamilolib.cli import CLIOptions

read_password = getpass.getpass

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')


def run_command(args, input_text):
    p = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE, text=True)
    stdout, stderr = p.communicate(input_text)
    print(stderr, end='')
    return stdout


class TestSetGetIntro(unittest.TestCase):
    # Test using run_command to check the behavior and the CLI at the
    # same time.
    def set_intro(self, filename):
        cmd = [SCRIPT, 'set-intro',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               filename]
        # print(' '.join(cmd))
        return run_command(cmd, self.password)

    def get_intro(self):
        cmd = [SCRIPT, 'get-intro',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               ]
        # print(' '.join(cmd))
        return run_command(cmd, self.password)

    def testSetGet(self):
        self.password = read_password()

        out = self.set_intro(os.path.join(TEST_DIR, 'intro.txt'))
        self.assertEqual(out, 'Intro was updated\n')

        intro = self.get_intro()
        # Some instance (campus.chamilo.org) add whitespaces around the text
        self.assertEqual(intro.strip(), 'This is the introduction text.')

        out = self.set_intro('/dev/null')
        self.assertEqual(out, 'Intro was deleted\n')

        intro = self.get_intro()
        self.assertEqual(intro.strip(), '')

        out = self.set_intro(os.path.join(TEST_DIR, 'intro2.txt'))
        self.assertEqual(out, 'Intro was updated\n')

        intro = self.get_intro()
        self.assertEqual(intro.strip(), 'This is another introduction text.')

if __name__ == "__main__":
    global o
    o = CLIOptions(stop_before_parse=True)
    known, unknown_args = o.parser.parse_known_args(
        ['get-intro'] + sys.argv[1:],
        namespace=o)
    if known.passwordFromStdin:
        def readline_and_strip():
            line = sys.stdin.readline()
            if line[-1] == '\n':
                return line[:-1]
        read_password = readline_and_strip
    unittest.main(argv=[sys.argv[0]] + unknown_args)
